import React, { Fragment, useState, useEffect } from 'react';
import Formulario from './components/Formulario';
import Cita from './components/Cita';

function App() {

  //Citas en local storage
  let citasIniciales = JSON.parse(localStorage.getItem('citas'));
  if(!citasIniciales) {
    citasIniciales = [];
  }

  //Array de citas
  const [citas, guardarCitas ] = useState(citasIniciales);

  //Use Effect para realizar ciertas operaciones cuando el state cambia
  //useEffect se ejecuta siempre que hay cambios en el componente o se recarga la página. para evitar que esto para evitar que esto pase se le pasa un array vacio. A este array se le conoce como array de dependencias
  useEffect( () => {
    if(citasIniciales) {
      localStorage.setItem('citas', JSON.stringify(citas));
    } else {
      localStorage.setItem('citas', JSON.stringify([]));
    }
  }, [citas, citasIniciales] );

  //Función que tome las citas actuales y agregue la nueva cita
  const crearCita = cita => {
    guardarCitas([...citas, cita]);
  }

  //función que elimina una cita por su id

  const eliminarCita = id => {
    const nuevasCitas = citas.filter(cita => cita.id !== id);
    guardarCitas(nuevasCitas);
  }

  return (
    <Fragment>
      <h1>Administrador de pacientes</h1>

      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario 
              crearCita={crearCita}
            />
          </div>
          <div className="one-half column">
            {citas.length === 0 ? <h2>No hay citas</h2> : <h2>Administra tus citas</h2>}
            {citas.map(cita => (
              <Cita
                key={cita.id}
                cita={cita}
                eliminarCita={eliminarCita}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
